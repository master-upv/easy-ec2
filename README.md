Description
===========
Simple utility to easily manage aws ec2 instances from command line
The project includes "The ec2 script" (One scripot to rule them all)
This "wrapper" to AWS CLI provides a simplified syntax to filter,
get information and manage ec2 instances.

Copyright & License
===================
Copyright: (c) 2021    Luis Garcia Gisbert, Sandra Villanueva Gavino

License: GPL-3+

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
   on any Debian system you can find a copy of the newest GNU GPL at
   '/usr/share/common-licenses/GPL'


Installation
============
The project contains a single script (ec2) with no additional dependencies
apart from gnu basic tools (grep, sed, tr, printf, ...) available in any
linux distribution (package coreutils in Debian/Ubuntu) and, certainly,
the AWS CLI (it must be properly installed and configured)

To do a basic installation, you only need to copy ec2 script into a directory
under your search PATH, and check the execution permissions of file,i.e.

  cp bin/ec2 /usr/local/bin

  chmod +x /usr/local/bin/ec2

Packaging in Debian/Ubuntu
--------------------------
The repository includes under directory "debian" all the necessary files to build a .deb package.
You must follow these steps:
 - Install (if they are not already installed) the packages build-essential & debhelper:
       apt install build-essential debhelper
 - Clone the git directory
 - cd to project directory and run "dpkg-checkbuilddeps" to detect additional build dependencies and
   use "apt install" to get the required packages if any (none for easy-ec2)
 - run this command:

   dpkg-buildpackage -rfakeroot -uc -us -sa

   Look in ../ directory to find the resulting .deb package and install with "dpkg -i"

 - run "debian/rules clean" to remove temporary files under debian directory.

Enjoy!


